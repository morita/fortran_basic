
## 環境構築

環境構築を行います。

### Windows の場合

#### 1. Windows Subsystem for Linux 2 のインストール

Windows 公式に、Windows Subsystem for Linux 2 という機能が Ubuntu に相当する提供されています。
以下のリンクを参考に、インストールしてください。

[Windows 10 用 Windows Subsystem for Linux のインストール ガイド](https://docs.microsoft.com/ja-jp/windows/wsl/install-win10)

#### 2. プログラム開発環境の構築

プログラム開発のため、以下のライブラリのインストールを行います。

- gcc (gfortran)
- make
- cmake
- MPI
- gnuplot
- git

Ubuntu 環境では、以下のコマンドでインストールできます。

```bash
$ sudo apt update
$ sudo apt upgrade
$ sudo apt install -y build-essential
$ sudo apt install -y cmake
$ sudo apt install -y gfortran
$ sudo apt install -y openmpi-doc openmpi-bin libopenmpi-dev
$ sudo apt install -y gnuplot
$ sudo apt install -y git
```

#### 3. Gitlab アカウント登録

森田研では、プログラムのバージョン管理システム git の web ホスティングサービスである Gitlab を利用します。

- [Gitlab](https://gitlab.com/) に登録し、Gitlab アカウント（先頭に@がある id）を森田に共有してください。
- [Gitlab SSH keys](https://gitlab.com/-/profile/keys) ページで、SSH 公開鍵を登録してください（[詳細はこちら](./01c_gitlab_ssh.md)）。

#### 4. 可視化ソフトのインストール

計算結果を可視化する可視化ソフト [Paraview](https://www.paraview.org/) をインストールしてください。

#### 5. 組版処理システム Latex のインストール

論文執筆のため、組版処理システム Latex をインストールしてください。
Ubuntu 環境では、以下のコマンドでインストールできます。

```bash
$ sudo apt install -y texlive-lang-cjk xdvik-ja evince
$ sudo apt install -y texlive-fonts-recommended texlive-fonts-extra
```
