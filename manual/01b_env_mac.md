
## 環境構築

環境構築を行います。

### macOS の場合

#### 1. Homebrew のインストール

ターミナルアプリがデフォルトの機能として提供されています。
プログラム開発のための、ライブラリ管理ツールとして、Homebrew のインストールを行います。

[Homebrew](https://brew.sh/index_ja)

#### 2. プログラム開発環境の構築

プログラム開発のため、以下のライブラリのインストールを行います。

- gcc (gfortran)
- make
- cmake
- MPI
- gnuplot

macOS 環境では、以下のコマンドでインストールできます。

```bash
$ brew update
$ brew upgrade
$ brew install gcc
$ brew install cmake
$ brew install gnuplot
$ brew install open-mpi
$ brew install gnuplot
$ brew install git
```

#### 3. Gitlab アカウント登録

森田研では、プログラムのバージョン管理システム git の web ホスティングサービスである Gitlab を利用します。

- [Gitlab](https://gitlab.com/) に登録し、Gitlab アカウント（先頭に@がある id）を森田に共有してください。
- [Gitlab SSH keys](https://gitlab.com/-/profile/keys) ページで、SSH 公開鍵を登録してください（[詳細はこちら](./01c_gitlab_ssh.md)）。

#### 4. 可視化ソフトのインストール

計算結果を可視化する可視化ソフト [Paraview](https://www.paraview.org/) をインストールしてください。

#### 5. 組版処理システム Latex のインストール

論文執筆のため、組版処理システム Latex をインストールしてください。
macOS 環境では、以下のコマンドでインストールできます。

```bash
$ brew install ghostscript
$ brew install gnupg2
$ brew tap caskroom/cask
$ brew cask install basictex
```
