
## Gitlab への SSH 公開鍵の登録

Gitlab への SSH 公開鍵の登録を行います。
SSH 技術は、ネットワークに繋がったコンピュータ同士が、安全に通信するための技術です。
ここでは、SSH 接続を行うための鍵認証の準備をします。

### SSH 認証鍵の作成

SSH 認証の公開鍵と秘密鍵を作成します。
ターミナルで、以下のコマンドを実行します。
```bash
$ cd ~/.ssh
$ ssh-keygen -t rsa -C {自分のメールアドレス@tsukuba.ac.jp}
```

このコマンドを実行すると、SSH 鍵を復号する際のパスワードの設定が求められるので、任意のパスワードを入力します。
ここで、パスワードを入力するとき、文字列を入力しても画面は変化しません。
パスワードの入力が終わったら、`ls` コマンドを実行すると、以下の 2 つのファイルが作成されているはずです。

- id_rsa： 秘密鍵、他人に見られないようにする
- id_rsa.pub： 公開鍵、GitLab に登録する

ここで、`id_rsa` を秘密鍵、`id_rsa.pub` を公開鍵と呼びます。
SSH 鍵認証は、よく「鍵」と「錠前」に例えられます。

秘密鍵は「鍵」に相当するもので、自分のコンピュータからネットワーク上のコンピュータ（例えば大学のスーパーコンピュータ）に
アクセスするときの鍵になります。そのため、鍵の内容を他人に見られないようにする必要があり、取り扱いには十分注意してください。

他方、公開鍵は「錠前」に相当するもので、ネットワーク上のコンピュータにある自分のアカウントを暗号化する錠前になります。
公開鍵を設定すると、ネットワーク上のコンピュータへアクセスする経路に、錠前が掛かった状態になります。
この錠前の解除には「鍵」、すなわち秘密鍵が必要になります。
「錠前」自体は誰にでも見られてよいものなので、

- サーバー管理者に自分の公開鍵を渡し、管理者が管理者権限により、自分のアカウントに公開鍵を設定する

ということができます。この手順は、研究室で運用されているような計算サーバーのアカウント設定時によく行われます。
スーパーコンピュータでは、この手順がウェブシステムにより自動されていることがほとんどです。

### Gitlab への登録

公開鍵が作成されたら、Gitlab に登録しましょう。
[Gitlab SSH keys](https://gitlab.com/-/profile/keys) ページから設定できます。
具体的には、`id_rsa.pub` の中身を `key` フォームにコピーするだけです。
この手順で、自分のコンピュータから Gitlab 上の自分のアカウントにアクセスできるようになりました。

