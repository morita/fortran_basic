
## git によるコード管理

### リポジトリのコミット 1

#### 1. `fortran_basic_template` リポジトリの説明

前回までにクローンした `fortran_basic_template` リポジトリを例に、
プログラムを開発し、開発した成果をコミットしてみましょう。

はじめに、自分が今どのディレクトリに位置しているか確認します。
`pwd` コマンドを実行すると、
```bash
$ pwd
```
以下のように
```bash
/home/{自分のアカウント名}/fortran_basic_template
```
自分がどのディレクトリに位置しているかがわかります。

それでは `cd` コマンドを使って `fortran_basic_template` ディレクトリにアクセスしてください。
上記のような出力がされていれば、既に `fortran_basic_template` ディレクトリにアクセスできています。

`ls` コマンドでディレクトリの中身を確認してみると、
```bash
$ ls
```
以下のように
```bash
00_hello_world 01_pi          README.md
```
現在は 2 つのフォルダと 1 つのファイルが存在していることがわかります。

#### 2. `00_hello_world` プログラムの更新

`00_hello_world` ディレクトリに移動しましょう。ここには、
```bash
00_hello_world.f90
Makefile
```
という 2 つのファイルがあらかじめ用意されています。

`00_hello_world.f90` の中身は
```fortran
program main

end program main
```
のようになっていて、最低限 fortran 言語の体裁は整っているものの、プログラムが何も書かれていない空っぽの状態になっています。

それでは、このプログラムを開発しましょう。
`00_hello_world.f90` の中身を
```fortran
program main
  write(*,*)"hello world!"
end program main
```
のように更新します。fortran 言語の書き方については、4 章以降を参照してください。

プログラムが正しく動作するか確認してみましょう。
ここでは、`Makefile` というプログラムをコンパイルする手順が指定されたファイルを使ってプログラムをコンパイルします。
`make` コマンドを実行すると `Makefile` ファイルが自動で参照され、コンパイルが実行されます。
```bash
$ make
```
例えば、
```bash
gfortran -o 00_hello_world 00_hello_world.f90
```
のようなログが出力されます。

その後、`ls` コマンドを実行し、`00_hello_world` というプログラムの実態が生成されていれば成功です。
`00_hello_world` プログラムを実行してみましょう。
```bash
./00_hello_world
```
`hello world!` と表示されました。
```bash
 hello world!
```
