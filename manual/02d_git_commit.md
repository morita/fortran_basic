
## git によるコード管理

### リポジトリのコミット 2

#### 3. 更新内容の確認

git はプログラムのバージョン管理を行うシステムです。
`git status` コマンドによって、どのファイルが更新されたかを確認することができます。
```bash
$ git status
```
と実行すると、例えば以下のように
```bash
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
    modified:   00_hello_world.f90

no changes added to commit (use "git add" and/or "git commit -a")
```
`00_hello_world.f90` ファイルが更新されたことがわかります。

詳しい差分を見る場合は、`git diff` コマンドを使います。
```bash
$ git diff
```
と実行すると、例えば以下のように
```bash
diff --git a/00_hello_world/00_hello_world.f90 b/00_hello_world/00_hello_world.f90
index ecd9d95..d39d404 100644
--- a/00_hello_world/00_hello_world.f90
+++ b/00_hello_world/00_hello_world.f90
@@ -1,4 +1,4 @@
 program main
-
+  write(*,*)"hello world!"
 end program main
```
`00_hello_world.f90` ファイルに `write(*,*)"hello world!"` という行が追加されたことがわかります。

#### 4. 更新ファイルの選択

ここまでの操作は、git コマンドによってどんなファイルがどんな内容で修正されたかを確認するという内容でした。
ここからは、この差分をプログラムの更新履歴としてコミットしましょう。
コミットは、その時点のプログラムを保存することと捉えればわかりやすいかと思います。

はじめに、`git add` コマンドにより、コミットするファイルを選択します。
ここでは、`00_hello_world.f90` ファイルを選択します。`00_hello_world` ディレクトリで以下を実行してみましょう。
```bash
$ git add 00_hello_world.f90
```
再度、
```bash
$ git status
```
と実行すると、例えば以下のように
```bash
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
    modified:   00_hello_world.f90
```
`00_hello_world.f90` ファイルがコミットする対象になったと表示されます。

上記で実行した以下のコマンドは、ファイル名を指定するものでした。
```bash
$ git add 00_hello_world.f90
```
複数のファイルを更新した場合、全てのファイルを選択には `.` を入力することで全てのファイルをコミットできます。
```bash
$ git add .
```

#### 4. 更新内容のコミット

それでは、`git add` コマンドで選んだ更新ファイルをコミットしましょう。
以下のように `git commit` コマンドを実行すると
```bash
$ git commit -m "add to wirte hello world"
```
次のようなログが出力されます。
```bash
[master 13f6459] add to write hello world
 1 file changed, 1 insertion(+), 1 deletion(-)
```
ここで、`git` コマンドのオプション `-m` はコミットの内容を記載する更新ログを入力します。

また、`-m` オプションを用いないで実行すると
```bash
$ git commit
```
以下のように画面が切り替わります。
```bash
# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
#
# On branch master
# Your branch is up to date with 'origin/master'.
#
# Changes to be committed:
#       modified:   00_hello_world.f90
#
```
この画面は、コミットの内容を記載する更新ログを作成する画面です。
この画面でも、例えば以下のように、どんなコミットをしたかを記述しましょう。
```bash
add to write 'hello world'
# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
#
# On branch master
# Your branch is up to date with 'origin/master'.
#
# Changes to be committed:
#       modified:   00_hello_world.f90
#
```
ただし、この画面は `vi` というテキストエディタです。
`vi` の使い方はここでは割愛します。

#### 5. 更新内容の反映

`git commit` コマンドを実行したあと、`git status` コマンドでファイルの状況を確認しましょう。
```bash
$ git status
```
と実行すると、例えば以下のように
```bash
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
```
'origin/master' という名前のブランチに 1 つのコミットがあったことがわかります。

それでは最後に、`git push` コマンドを使ってプログラムの更新を gitlab のシステムに伝えましょう。
```bash
$ git push
```
このコマンドを実行すると、以下のようなログが出力されます。
```bash
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 8 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 410 bytes | 410.00 KiB/s, done.
Total 4 (delta 1), reused 0 (delta 0)
To https://gitlab.com/nmorita.kz/fortran_basic_template.git
   43972e8..5f53bda  master -> master
```

gitlab のページから、`Repository` → `Graph` のようにアクセスすると、
プログラムの更新履歴を確認することができます。

![fig2.1](./02/05.png)
![fig2.1](./02/08.png)

#### 6. まとめ

ここまでに出てきた git コマンドは以下の通りです。

- `git clone`：リポジトリを自分のコンピュータにコピーする
- `git status`：ファイル変更の状態を確認する
- `git add`：変更をしたファイルを選択する
- `git commit -m "comment"`：変更内容をコミットする
- `git push`：自分のコンピュータで開発した内容で gitlab システム上のデータを更新する

以上のコマンドを利用することで、適宜開発履歴を残しながらプログラムの更新を行いましょう。

