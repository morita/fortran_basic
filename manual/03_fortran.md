
## Fortran の基礎

### 動作環境の確認

はじめに
```bash
$ which gfortran
```
と実行してください。実行の結果、
```bash
/usr/local/bin/gfortran
```
のように、gfortran コマンドの実態がいずれかのディレクトリに存在していることを確認してください。
何も出力されない場合、gfortran コマンドがインストールされていません。
計算機環境の構築が正しくなされているか、再度確認してください。

### Fortran コード記法

fortran 言語のコンパイルをしてみましょう。

- [主プログラムと hello world](./03a1_fortran.md)
- [暗黙の型宣言と変数](./03a2_fortran.md)
- [if 文による条件分岐](./03a3_fortran.md)
- [do ループによる繰り返し処理](./03a4_fortran.md)
- [サブルーチンと関数](./03a5_fortran.md)
- [多次元配列と動的確保](./03a6_fortran.md)
- [ファイル入出力](./03a7_fortran.md)
- [構造体（構造型）](./03a8_fortran.md)
- [モジュールとプログラムのリンク](./03a9_fortran.md)
- [バグのないプログラムを作成するために](./03a10_fortran.md)

### Fortran 演習

- [Hello world](./03b_fortran_compile.md)
- [演習 (1)](./03c_fortran_pi.md)

