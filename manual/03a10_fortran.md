
## Fortran の基礎

### 10. バグのないプログラムを作成するために

バグのないプログラムを作成するために、開発時には

```fortran
$ gfortran -fbounds-check -fbacktrace -Wuninitialized -ffpe-trap=invalid,zero,overflow
```

とコンパイルオプションを設定してコンパイルすることを推奨する。
ここでは、

- -fbounds-check：多次元配列にアクセスする際に配列長を越えたアクセスがなされていないか境界チェックを行う
- -fbacktrace：エラーが発生した場合に、ソースプログラムの当該箇所を出力する
- -Wuninitialized： コンパイル時に初期化がなされていないときワーニング出力をする
- -ffpe-trap=invalid,zero,overflow：ゼロ割など定義されていない演算が生じたときにエラー出力をする

と指定している。

また、

- `parameter` 指定子の利用
- `private`、`public` 宣言の利用

をすることで、安全にプログラム開発ができる。
