
## Fortran の基礎

### 1. 主プログラムと hello world

fortran は、`program` と `end program` で囲まれた範囲がプログラムとなる。
この範囲を **主プログラム部** という。

```fortran
program main
  !> 主プログラム部
end program main
```

`!`で始まる行はコメントアウトとなる。
```fortran
program main
  ! これはコメントアウト
end program main
```

`print`文で文字列を出力できる。
print 文の `*` はターミナルに出力させることを意味する。

```fortran
program main
  !> print 文による出力
  print *, "hello world"
end program main
```

`""` で囲むことで文字列として扱われる。

コンパイルは gfortran の場合

```fortran
$ gfortran {file name}
```

と実行する。
コンパイルされるプログラム名前の指定がない場合、`a.out` というプログラムが生成される。
プログラムを実行する場合は、

```fortran
$ ./a.out
```
と実行する。
