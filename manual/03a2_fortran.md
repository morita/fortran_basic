
## Fortran の基礎

### 2. 暗黙の型宣言と変数

暗黙の型宣言とは、

```fortran
program main
  i = 1.0
end program main
```

のように、変数の型を宣言をせず、いきなり変数を利用できる記法である。
この場合、変数名の先頭が `i, j, k, l, m, n` で始まる変数は、整数変数と扱われる。
fortran は本来、型宣言が必要なプログラミング言語であるが、
暗黙の型宣言は、計算機のメモリ容量が潤沢に備わっていない時代の名残りである。
**暗黙の型宣言は、バグの温床であり、絶対に利用してはいけない。**

そこで、主プログラムの先頭に `implicit none` と記述する。

```fortran
program main
  implicit none
  i = 1.0 !> これはコンパイルエラーとなる
end program main
```

fortran には以下の 4 つの型がある。
- 整数型 (32 bit/64 bit)
- 浮動小数点数型 (32 bit/64 bit)
- 論理型
- 文字列型

それぞれ、主プログラム部分の先頭に以下のように定義する。

```fortran
program main
  implicit none
  integer(4) :: i, j
  integer(8) :: n, m
  real(4) :: a, b
  real(8) :: e, f
  logical :: flag
  character :: c*10
end program main
```

実用的には、

- 32 bit 整数型
- 64 bit 浮動小数点数型

を利用すれば差し支えない。

また、`parameter` 指定子を使えば、プログラム中で定数を宣言できる。

```fortran
program main
  implicit none
  integer(4), parameter :: i = 1
end program main
```

このとき、

```fortran
program main
  implicit none
  integer(4), parameter :: i = 1
  i = 2
end program main
```

のようにパラメータの値を更新しようとすると、コンパイルエラーになるため、安全にプログラム開発ができる。
