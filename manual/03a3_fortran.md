
## Fortran の基礎

### 3. if 文による条件分岐

if 文は

```fortran
if(条件)then
  !> 条件が真のときの処理
endif
```

と記載する。複数条件がある場合は、

```fortran
if(条件 1)then
  !> 条件 1 が真のときの処理
elseif(条件 2)then
  !> 条件 2 が真のときの処理
endif
```

と書く。また、条件が偽のときに処理をする場合は、

```fortran
if(条件)then
  !> 条件が真のときの処理
else
  !> 条件が偽のときの処理
endif
```

と書ける。

条件に利用する評価式として、

```fortran
==
/=
<
<=
>
>=
```

が利用できる。

例えば、

```fortran
i = 1
if(i == 1)then
  !> この中の処理が実行
  print *, "here"
endif
```

などと書ける。また、この単純な if 文は、以下のように 1 行に短縮して書ける。

```fortran
  if(i == 1) print *, "here"
```

条件を複数定義したい場合は、以下の論理比較式を利用できる。

```fortran
.and.
.not.
.or.
```

例えば、以下の例では条件が真となり、print 文が実行される。

```fortran
i = 1
j = 2
if(i == 1 .and. j == 2)then
  !> この中の処理が実行
  print *, "here"
endif
```
