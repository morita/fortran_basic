
## Fortran の基礎

### 4. do ループによる繰り返し処理

do ループは

```fortran
do i = 1, 10
  !> 繰り返し処理
enddo
```

と記載する。例えば、

```fortran
integer :: i, sum
sum = 0
do i = 1, 10
  sum = sum + i
enddo
print *, sum
```

と実行すると、1 から 10 の和が計算され `55` と出力される。
do ループは、`exit`文によりループを終了でき、`cycle`文により当該のループをスキップできる。
