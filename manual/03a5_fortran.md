
## Fortran の基礎

### 5. サブルーチンと関数

主プログラムに対し、`contains` 文を追加すると、
それ以降のプログラムは副プログラム部として定義される。

```fortran
program main
  !> 主プログラム部
contains
  !> 副プログラム部
end program main
```

副プログラム部には、サブルーチンおよび関数を定義できる。

サブルーチンは、

```fortran
subroutine sub_name(a, b, c)
  implicit none
  integer :: a, b, c
  c = a + b
end subroutine sub_name
```

のように定義する。ここで、`a`、`b`、`c` は引数である。
主プログラムからは、

```fortran
call sub_name(a, b, c)
```

のように呼べる。

関数は、

```fortran
function func_name(a, b)
  implicit none
  integer :: a, b, func_name
  func_name = a + b
end function func_name
```

のように定義する。ここで、`a`、`b` は引数である。
特に、関数名の戻り値を指定するために、宣言部に関数名と同名の変数を定義する。
主プログラムからは、

```fortran
c = func_name(a, b, c)
```

のように呼べる。
