
## Fortran の基礎

### 8. 構造体（構造型）

構造体の宣言は、変数宣言部において以下のように行う。

```fortran
!> 構造体の定義
type struc_param
  implicit none
  integer :: n
end type struc_param

!> 構造体変数の宣言
type(struc_param) :: param
```

構造体がもつ変数にアクセスする場合は、`%`を使う。

```fortran
n = param%n
```
