
## Fortran の基礎

### 9. モジュールとプログラムのリンク

モジュールは、サブルーチンを別ファイルにまとめたものである。

```
module utils
!> 宣言部
  implicit none

contains
!> モジュール副プログラム部
  subroutine add(a, b, c)
    implicit none
    integer :: a, b, c
    c = a + b
  end subroutine add
end module
```

モジュールに定義された関数を利用するときは、メインプログラムから、

```
program main
  use utils
  implicit none
  integer :: a, b, c

  call add(a, b, c)
end program main
```
と呼ぶ。`use` 文を使って読み込むモジュールを指定することで、モジュール化された見通しのよいプログラムを作成できる。

コンパイルするときは、モジュールが定義されたファイルを、
```
$ gfortran -c util.f90
```
のようにコンパイルする。その結果、`util.o` と `util.mod` が生成される。
メインプログラムをコンパイルするときには、
```
$ gfortran main.f90 util.o
```
のように、プログラムをリンクする。
