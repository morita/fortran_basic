
## Fortran の基礎：コンパイル

### 練習 (1)：「hello world」と印字せよ

はじめてプログラム言語を学ぶ場合、動作する最小のプログラムを作成して、コンパイル・実行を行います。
その中で最も広く知られているサンプルプログラムの一つが `hello world` です。
それでは「hello world」と画面に出力してみましょう。

[00_hello_world](https://gitlab.com/morita/fortran_basic_template/-/tree/master/00_hello_world)
の
`00_hello_world.f90`
を更新してください。

**途中経過 1：** 主プログラムを定義した `00_hello_world.f90`

fortran は、プログラムの最初を `program main`、プログラムの最後を `end program main` で囲みます。
この部分を「主プログラム」と呼びます。
`main` という部分は、任意の文字列を指定することが可能です。
ここでは、主プログラム名を `main` とし、ファイル名は `00_hello_world.f90` としています。

```fortran
program main

end progam main
```

**途中経過 2：** write 関数を追加した `00_hello_world.f90`

次に、画面に文字列を出力することを考えます。
fortran では、`write` 関数を利用して、画面に文字列を出力することができます。

```fortran
program main
  write(*,*)"hello world"
end progam main
```

プログラムが更新できたら、
```bash
$ make
$ ./00_hello_world
```
と実行してみましょう。
ターミナルに
```bash
 hello world
```
と表示されました。
