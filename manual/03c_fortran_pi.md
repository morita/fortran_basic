
## 課題 (1)：円周率を推定するプログラム

長さ 1 の正方形と半径 1、中心角 90 度の扇型が図のように重なっています。

<img src="./03/pi.svg" height=350px>

それぞれの面積は以下のように表されます。

$$ S_{\rm square} = 1.0 $$

$$ S_{\rm circle} = \frac{1}{4} \pi r^2 $$

この関係から、乱数を用いて円周率を推定するプログラムを作成してください。
fortran において、乱数は `random_number` 関数から取得できます。
横軸を \\( x \\) 軸、縦軸を \\( y \\) 軸として、試行点の座標 (\\( x, y \\)) を乱数から決定し、
\\( S_{\rm square} \\) および \\( S_{\rm circle} \\) に含まれる試行点の個数の比から円周率が推定できます。

### 作成プログラム

[01_pi](https://gitlab.com/morita/fortran_basic_template/-/tree/master/01_pi)
の
`01_pi.f90`
を更新してください。

### 入力

```
円周率を推定する乱数の生成数（試行回数）
```

### 出力

```
推定円周率
```

