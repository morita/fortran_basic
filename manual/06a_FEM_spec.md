
## 有限要素解析プログラムの仕様

### 機能

- 2 次元四角形一次要素
- 数値積分
- 共役勾配法（CG 法）による連立一次方程式求解

### 検証方法

- 円孔無限平板の理論解との比較

### 入力フォーマット

- 次ページを参考にしてください。

### 出力フォーマット

出力フォーマットは、Paraview で可視化できるフォーマットで出力してください。
以下の 2 つをお薦めします。

- Paraview 向け可視化ファイル
    - UCD INP 形式（拡張子 inp）
    - VTK 形式（拡張子 vut）

### プログラム作成の方針

0. 仮想仕事の原理および Galerkin 法により、支配方程式を弱形式化する
1. 入力ファイル例を作成する
2. ファイルを読み込み、可視化ファイルを出力する
3. 出力ファイルを Paraview で可視化する
4. 要素剛性行列を作成する
5. 全体剛性行列を作成する
6. 境界条件、荷重条件を付与する
7. 線形ソルバ（共役勾配法）を作成する
8. ひずみ、応力を計算する
9. 理論解を得る
10. 精度検証用の入力ファイルを作成する
11. 理論解と比較する

### 検証方法の例

#### 数値積分

例えば、数値積分の機能の一部については以下のように検証できます。

- 面積を計算する

#### 線形ソルバ

例えば、線形ソルバについては以下のように検証できます。

- x' = 1 とおく
- y = Ax' を計算する
- Ax = y を解く
- x と x' を比較する

#### 計算結果の評価

まずは、任意の境界条件に対して定性的に妥当な結果かどうか検討してみましょう。
次に、定量的に妥当な結果かどうかは、以下のように検証できます。

- Verification & Validation (V&V) について調査する
- 理論解と数値解を比較し誤差を得る
- 要素分割数と誤差との関係を検討する

